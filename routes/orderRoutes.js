const express = require('express');
const router = express.Router();

const{
    checkOut,
    getAllOrders,
    userOrders,
    checkOrder,
    orderStatus,
    addOrders,
    deleteOrder
} = require('./../controllers/orderControllers')

//User authentication
const {
    verify,
    decode,
    verifyAdmin
} = require('./../auth');

//Non-admin User checkout (Create Order)
router.post('/checkout', verify, async(req, res) =>{
    // console.log("test @routes")
    const {id, isAdmin} = decode(req.headers.authorization)

    const data = {
        userId: id,
        totalAmount: req.body.totalAmount,
        transactions: req.body.transactions
    }

    console.log(data);

    // console.log(data)
    if(isAdmin){
        res.send(`Only customers can access this page!`)
    }
    else{
        try{
            await checkOut(data).then(result => res.send(result))
        }
        catch(err){
            res.status(500).json(err)
        }
    }
    
})

//Add products if order already exists and active
router.patch('/add-products', verify, async(req, res) =>{
    // console.log("test @routes")
    const {id, isAdmin} = decode(req.headers.authorization)

    const data = {
        userId: id,
        productId: req.body.productId,
        quantity: req.body.quantity
    }

    // console.log(data)
    if(isAdmin){
        res.send(`Only customers can access this page!`)
    }
    else{
        try{
            await addOrders(data).then(result => res.send(result))
        }
        catch(err){
            res.status(500).json(err)
        }
    }
    
})

//remove a product on the order list
router.delete('/delete-product', verify, async(req, res) =>{
     // console.log("test @routes")
     const {id, isAdmin} = decode(req.headers.authorization)

     const data = {
         userId: id,
         productId: req.body.productId
     }
 
     // console.log(data)
     if(isAdmin){
         res.send(`Only customers can access this page!`)
     }
     else{
         try{
             await deleteOrder(data).then(result => res.send(result))
         }
         catch(err){
             res.status(500).json(err)
         }
     }
})

// Retrieve all orders (Admin only)
router.get('/orders', verifyAdmin, async (req, res) =>{
    // console.log("test @routes")

    try{
        await getAllOrders().then(result => res.send(result))
    }
    catch(err){
        res.status(500).json(err)
    }
})

//Retrieve authenticated user’s orders
router.get('/myOrders', verify, async (req, res) =>{
    const {id, isAdmin} = decode(req.headers.authorization)

    if(isAdmin){
        res.send(`Only customers can access this page!`)
    }
    else{
        try{
            await userOrders(id).then(result => res.send(result))
        }
        catch(err){
            res.status(500).json(err)
        }
    }
})

//Check if Order exist
router.get('/order-exist', verify, async (req, res) =>{
    const {id, isAdmin} = decode(req.headers.authorization)

    if(isAdmin){
        res.send(`Only customers can access this page!`)
    }
    else{
        try{
            await checkOrder(id).then(result => res.send(result))
        }
        catch(err){
            res.status(500).json(err)
        }
    }
})

//Change Order status if complete
router.patch('/complete', verify, async (req, res) =>{
    const {id, isAdmin} = decode(req.headers.authorization)

    if(isAdmin){
        res.send(`Only customers can access this page!`)
    }
    else{
        try{
            await orderStatus(id).then(result => res.send(result))
        }
        catch(err){
            res.status(500).json(err)
        }
    }
})

//Export the router module to be used in index.js file
module.exports = router;