const express = require('express');
const res = require('express/lib/response');
const {
    verify,
    decode,
    verifyAdmin
} = require('../auth');
const router = express.Router();

const{
    addProduct,
    activeProducts,
    oneProduct,
    updateProduct,
    archiveProduct,
    unarchiveProduct
} = require('./../controllers/productControllers')

// Create Product (Admin only)
router.post('/', verifyAdmin, async (req, res) =>{
    //console.log("test @routes")
    try{
        await addProduct(req.body).then(result => res.send(result))
    }
    catch(err){
        res.status(500).json(err)
    }
})

// Retrieve all active products
router.get('/', verify, async (req, res) =>{
    // console.log("test @routes")
    const isAdmin = decode(req.headers.authorization).isAdmin
    // console.log(isAdmin)
    try{
        await activeProducts(isAdmin).then(result => res.send(result))
    }
    catch(err){
        res.status(500).json(err)
    }
})

// Retrieve single product
router.get('/:productId', verify, async (req, res) =>{
    // console.log("test @routes")
    const prodId = req.params.productId

    try{
        await oneProduct(prodId).then(result => res.send(result))
    }
    catch(err){
        res.status(500).json(err)
    }
})

//Update Product information (Admin only)
router.put('/:productId', verifyAdmin, async (req, res) =>{
    // console.log("test @routes")
    const prodId = req.params.productId

    try{
        await updateProduct(prodId, req.body).then(result => res.send(result))
    }
    catch(err){
        res.status(500).json(err)
    }
})

//Archive a product
router.patch('/:productId/archive', verifyAdmin, async (req, res) =>{
    //  console.log("test @routes")
    const prodId = req.params.productId

    try{
        await archiveProduct(prodId).then(result => res.send(result))
    }
    catch(err){
        res.status(500).json(err)
    }
})

//Unarchive a product
router.patch('/:productId/unarchive', verifyAdmin, async (req, res) =>{
    //  console.log("test @routes")
    const prodId = req.params.productId

    try{
        await unarchiveProduct(prodId).then(result => res.send(result))
    }
    catch(err){
        res.status(500).json(err)
    }
})

//Export the router module to be used in index.js file
module.exports = router;