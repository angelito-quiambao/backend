
//import Schema
const Product = require('../models/Product');

// Create Product (Admin only)
module.exports.addProduct = async (reqBody) =>{
    // console.log("test @controllers")

    const {productName, description, price, quantity} = reqBody

    const newProduct = new Product({
        productName: productName,
        description: description,
        price: price,
        quantity: quantity
    })

    // console.log(newProduct)
    return await newProduct.save().then(result => {
        if(result){
            return true
        }
        else{
            if (result == null){
                return false
            }
        }
    })
}

// Retrieve all active products
module.exports.activeProducts = async (isAdmin) =>{
    // console.log("test @controllers")
    if(isAdmin){
        // console.log("true")
        return await Product.find().then(result => result)
    }
    else{
        // console.log("false")
        return await Product.find({isActive: true}).then(result => result)
    }
}

// Retrieve single product
module.exports.oneProduct = async (id) =>{
    // console.log("test @controllers")
    return await Product.findById(id).then(result =>{
        if(result){
            return result
        }
        else{
            return {message: `Product not existing`}
        }
    })
}

//Update Product information (Admin only)
module.exports.updateProduct = async (id, reqBody) =>{
    // console.log("test @controllers")
    return await Product.findByIdAndUpdate(id, {$set: reqBody}, {new: true}).then((result, err) =>{
        if(result){
            return result
        }
        else{
            return {message: `Product not existing`}
        }

    })
}

//Archive a product
module.exports.archiveProduct = async (id) =>{
    // console.log(id)
    // console.log("test @controllers")
    return await Product.findByIdAndUpdate(id, {$set:{isActive: false}}, {new: true}).then((result, err) =>{

        if(result){
            return result
        }
        else{
            return {message: `Product not existing`}
        }

    })
}

//Unarchive a product
module.exports.unarchiveProduct = async (id) =>{
    // console.log(id)
    // console.log("test @controllers")
    return await Product.findByIdAndUpdate(id, {$set:{isActive: true}}, {new: true}).then((result, err) =>{

        if(result){
            return result
        }
        else{
            return {message: `Product not existing`}
        }

    })
}