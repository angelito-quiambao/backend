//inlude packages for project development
const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv').config();
const cors = require('cors');

const PORT = process.env.PORT || 3008;
const app = express();

//route modules connection
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes');

//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors())

//Connect Database to sever
mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true});

//Test DB connection
const db = mongoose.connection
db.on("error", console.error.bind(console, 'connection error:'));
db.once("open", () => console.log(`Connected to Database`));


//ROUTES
app.use(`/api/users`, userRoutes);
app.use(`/api/products`, productRoutes);
app.use(`/api/orders`, orderRoutes);



app.listen(PORT, () => console.log(`Server connected to ${PORT}`));